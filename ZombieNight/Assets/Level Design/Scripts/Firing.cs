using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Firing : MonoBehaviour
{
    AudioSource audioS;
    public AudioClip clip;
    public ParticleSystem ps;
    public Transform gun;
    public float recoil;
    Ray ray;
    RaycastHit hit;
    public Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        audioS = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            audioS.PlayOneShot(clip);
            ps.Play();
            StartCoroutine("Recoil");

            ray = cam.ScreenPointToRay(Input.mousePosition);

            if(Physics.Raycast(ray, out hit))
            {
                if(hit.collider.gameObject.tag == "Zombie")
                {
                    hit.collider.gameObject.GetComponent<ZombieBehaviour>().Dying();
                    hit.collider.enabled = false;
                }
            }


        }
    }
    IEnumerator Recoil()
    {
        gun.transform.Rotate(Vector3.right * recoil);
        yield return new WaitForSeconds(0.1f);
        gun.transform.Rotate(Vector3.left * recoil);
    }
}
