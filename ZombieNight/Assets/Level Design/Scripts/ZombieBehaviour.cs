using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieBehaviour : MonoBehaviour
{
    NavMeshAgent agent;

    public GameObject player;
    Animator animator;
    Animator playerAnim;
    bool isDead;
    public CharacterMovement playerMovement;
    public Firing playerFiring;
    public MouseLook playerLook;
    public GameObject damageImage;
    public AudioSource playerSource;
    public AudioClip playerDeath;
    Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        playerAnim = player.GetComponent<Animator>();
        playerSource = player.GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
        if (!isDead)
        {
            agent.destination = player.transform.position;
        }
        if (isDead)
        {
            agent.isStopped = true;
        }
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Attacking"))
        {
            StartCoroutine("Kill");
        }


    }
    public void Dying()
    {
        isDead= true;
        animator.SetTrigger("dead");
        Destroy(gameObject, 3);
        rb.constraints = RigidbodyConstraints.FreezePosition;
    }
    void playerDying()
    {
        playerMovement.enabled = false;
        playerFiring.enabled = false;
        playerLook.enabled = false;
        playerSource.PlayOneShot(playerDeath);
        damageImage.SetActive(true);
        playerAnim.SetTrigger("dead");
    }
    //void OnCollisionEnter(Collision other)
    //{
    //    if(other.gameObject == player)
    //    {
    //        animator.SetTrigger("attack");
    //        StartCoroutine("playerDying");
    //        agent.isStopped = true;
    //    }
    //}
    //private void OnCollisionEnter(Collision collision)
    //{
    //    if(collision.gameObject == player)
    //    {
    //        animator.SetTrigger("attack");
    //        agent.isStopped = true;
    //        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Attacking"))
    //        {
    //            agent.isStopped = false;
    //            if(collision.gameObject == player)
    //            {
    //                print("no");
    //                StartCoroutine("playerDying");
    //            }
    //        }
    //    }   
    //}
    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject == player)
        {
            animator.SetBool("attack", true);
        }
    }

    IEnumerator Kill()
    {
        agent.isStopped = true;
        if(Vector3.Distance(transform.position, player.transform.position) < 1.5f)
        {
            yield return new WaitForSeconds(1);
            playerDying();
        }
        else
        {
            yield return new WaitForSeconds(1.5f);
            agent.isStopped = false;
            animator.SetBool("attack", false);
        }
    }
    

}
