using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    float horizontal;
    float vertical;
    public float horSpeed;
    public float vertSpeed;
    CharacterController CC;
    public float g = -9.8f;
    // Start is called before the first frame update
    void Start()
    {
        CC = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        CC.Move(transform.forward * vertSpeed * vertical + new Vector3(0, g, 0) + transform.right * horSpeed * horizontal);
    }
}
