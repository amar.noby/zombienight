﻿using UnityEngine;
using System.Collections;

public class ThunderClap : MonoBehaviour
{
	public AudioClip clip;
	bool canFlicker = true;
	AudioSource source;
	Light lightning;
	void Start()
    {
		source = GetComponent<AudioSource>();
		lightning = GetComponent<Light>();
	}
	void Update ()
	{
		StartCoroutine ("Flicker");
	}

	IEnumerator Flicker ()
	{
		if ( canFlicker )
		{
			canFlicker = false;
			source.PlayOneShot ( clip );
			lightning.enabled = true;
			yield return new WaitForSeconds ( Random.Range ( 0.1f, 0.4f ) );
			lightning.enabled = false;
			yield return new WaitForSeconds ( Random.Range ( 0.1f, 5 ) );
			canFlicker = true;
		}
	}
}
